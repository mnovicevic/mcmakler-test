<?php

namespace NeoBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;

/**
 *
 * decides which config is loaded in the container
 *
 * Class JoizHardcoreExtension
 * @package Joiz\HardcoreBundle\DependencyInjection
 */
class NeoExtension extends Extension {

    /**
     * Yaml config files to load
     * @var array
     */
    protected $resources = array(
        'config' => 'services.yml',
        'repository' => 'repository.yml'
    );

    /**
     * Loads the services based on your application configuration.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container) {

        $loader = $this->getFileLoader($container);
        $loader->load($this->resources['config']);
        $loader->load($this->resources['repository']);
    }

    /**
     * Get File Loader
     *
     * @param ContainerBuilder $container
     * @return \Symfony\Component\DependencyInjection\Loader\YamlFileLoader
     */
    public function getFileLoader(ContainerBuilder $container) {
        return new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
    }

}
