<?php

namespace NeoBundle\Services;

use NeoBundle\Entity\Neo;
use NeoBundle\Repository\NeoRepository;

class NeoGeneratorService {

    /** @var  NeoRestClient */
    protected $neoRestClient;

    /** @var  NeoRepository */
    protected $neoRepository;

    public function __construct(NeoRestClient $neoRestClient,
                                NeoRepository $neoRepository)
    {
        $this->neoRestClient = $neoRestClient;

        $this->neoRepository = $neoRepository;
    }


    /**
     * @return array
     */
    public function generate()
    {
        $neos = $this->neoRestClient->feedNeo();

        if(empty($neos['near_earth_objects'])) {
            return [];
        }

        foreach($neos['near_earth_objects'] as $date => $neoByDate) {
            foreach($neoByDate as $neo) {
                $neoObj = new Neo();
                $neoObj->setName($neo['name']);
                $neoObj->setDate(new \DateTime($date));
                $neoObj->setIsHazardous($neo['is_potentially_hazardous_asteroid']);
                $neoObj->setNeoReferenceId($neo['neo_reference_id']);

                // @TODO I'm not sure how speed is define
                $neoObj->setSpeed('100');

                $this->neoRepository->persist($neoObj);
            }
        }
        $this->neoRepository->flush();
    }

}