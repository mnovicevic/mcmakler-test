<?php

namespace NeoBundle\Services;

use Buzz\Client\Curl;
use Buzz\Message\Request;
use Buzz\Message\Response;

class NeoRestClient {

    /** @var  string */
    protected $apiKey;

    /** @var  Curl */
    protected $buzz;

    public function __construct($buzz,
                                $neoApiKey)
    {
        $this->apiKey = $neoApiKey;
        $this->buzz = $buzz;
    }


    /**
     * @param int $page
     * @param int $size
     * @return Response|mixed
     */
    public function browseNeo($page = 0, $size = 20)
    {
        $url = '/neo/rest/v1/neo/browse';
        $this->setApiKey($url);
        $this->setQueryParam($url, 'page', $page);
        $this->setQueryParam($url, 'size', $size);

        $request = new Request('GET', $url, 'https://api.nasa.gov');

        $response = new Response();

        $this->buzz->send($request, $response);
        $response = json_decode($response->getContent(), true);
        return $response;
    }

    /**
     *
     * @param null $startDate
     * @param null $endDate
     * @param bool $detailed
     * @return Response|mixed
     */
    public function feedNeo($startDate = null, $endDate = null, $detailed = true)
    {
        $url = '/neo/rest/v1/feed';

        $this->setApiKey($url);

        $startDate = $startDate ?: date('Y-m-d');
        $endDate = $endDate ?: date("Y-m-d", strtotime($startDate." -7 days"));

        $this->setQueryParam($rl, 'start_date', $startDate);
        $this->setQueryParam($url, 'end_date', $endDate);
        $this->setQueryParam($url, 'detailed', $detailed);

        $request = new Request('GET', $url, 'https://api.nasa.gov');

        $response = new Response();

        try {
            $this->buzz->send($request, $response);
            $response = json_decode($response->getContent(), true);
            return $response;
        } catch (\Exception $ex) {
            //@todo handle exception and log error
            return [];
        }


    }


    private function setQueryParam(&$url, $paramName, $param)
    {
        $url .= "&$paramName=$param";
    }

    private function setApiKey(&$url)
    {
        $url .= "?api_key=$this->apiKey";
    }
}