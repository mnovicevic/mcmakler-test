<?php

namespace NeoBundle\Controller;

use NeoBundle\Repository\NeoRepository;
use NeoBundle\Services\NeoRestClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

class NeoController extends Controller
{
    /**
     * @Route("/neo/hazardous")
     */
    public function indexAction()
    {
        /** @var NeoRestClient $neoRestClient */
        $neoRestClient = $this->get('neo_rest_client');
        return new JsonResponse($neoRestClient->feedNeo());

    }


    /**
     * @Get("/neo/hazardous")
     *
     */
    public function getHazardousAction()
    {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $this->get('neo.repository');
        return $neoRepository->findAllHazardous();
    }

    /**
     *
     * @Get("/neo/fastest")
     *
     * @Rest\QueryParam(
     *  name="hazardous",
     *  nullable=true,
     *  default=0,
     *  description="with a hazardous parameter, where true means is hazardous")
     *
     * @param $hazardous
     * @return array
     */
    public function getFasterNeoAction($hazardous = 0) {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $this->get('neo.repository');
        return $neoRepository->findFastestNeo($hazardous);
    }

    /**
     *
     * @Get("/neo/best-year")
     *
     * @Rest\QueryParam(
     *  name="hazardous",
     *  nullable=true,
     *  default=0,
     *  description="with a hazardous parameter, where true means is hazardous")
     *
     * @param $hazardous
     * @return array
     */
    public function getNeoBestYearAction($hazardous = 0) {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $this->get('neo.repository');
        return $neoRepository->findNeoBestYear($hazardous);
    }

    /**
     *
     * @Get("/neo/best-month")
     *
     * @Rest\QueryParam(
     *  name="hazardous",
     *  nullable=true,
     *  default=0,
     *  description="with a hazardous parameter, where true means is hazardous")
     *
     * @param $hazardous
     * @return array
     */
    public function getNeoBestMonthAction($hazardous = 0) {
        /** @var NeoRepository $neoRepository */
        $neoRepository = $this->get('neo.repository');
        return $neoRepository->findNeoBestMonth($hazardous);
    }
}
