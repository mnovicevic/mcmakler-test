<?php

namespace NeoBundle\Command;

use NeoBundle\Services\NeoGeneratorService;
use NeoBundle\Services\NeoRestClient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class NeoGeneratorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('Neo:Generator')
            ->setDescription('Generate Neo object by fetching them from nasa api')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument('argument');

        if ($input->getOption('option')) {
        }

        /** @var NeoGeneratorService $neoGenerator */
        $neoGenerator = $this->getContainer()->get('neo_service_generator');
        $neoGenerator->generate();

        $output->writeln('Done');
    }

}
