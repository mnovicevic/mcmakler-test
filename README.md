Neo Test Project
==========

###INSTALLATION
 - docker-compose build
 - docker-compose up 
 
 Run docker ps and check container id for web server then run:
 - docker exec -it <web_id> bash
 - app/console do:da:cr
 - app/console do:sc:up --force
 
##### MYSQL
  You can access to DB with phpmyadmin on following address
  - http://0.0.0.0:8080/

##### WEB SITE
   Web site is available on following address
   - http://0.0.0.0/web/app_dev.php
 


###API
All API response return JSON by default.

   Display all DB entries which contain potentially hazardous asteroids
   - /api/neo/hazardous
   
   calculate and return the model of the fastest asteroid
   - /api/neo/fastest?hazardous=(true|false)
   
   calculate and return a year with most ateroids
   - /api/neo/best-year?hazardous=(true|false)
   
   alculate and return a month with most ateroids (not a month in a year)
   - /api/neo/best-month?hazardous=(true|false)
   
